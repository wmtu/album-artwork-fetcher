# Album Artwork Fetcher

Python 3 script that pulls song data from the WMTU log api and fetches the album artwork for the currently playing song.

## Installation

Copy the ArtworkFetcher directory to a convenient place

```bash
cp -r ./ArtworkFetcher /opt/proxy/
```

Edit the config.ini file and ArtworkFetcherCheck.sh file to match your environment

```bash
nano /opt/proxy/ArtworkFetcher/config.ini
nano /opt/proxy/ArtworkFetcher/ArtworkFetcherCheck.sh
```

Add the checker script to crontab

```bash
crontab -e
```

```bash
# artwork fetch
* * * * * bash /opt/proxy/ArtworkFetcher/ArtworkFetcherCheck.sh
```

Copy the logrotate configuration file to the logrotate directory

```bash
sudo cp logrotate /etc/logrotate.d/artworkfetcher
```

Edit the logrotate configuration to match your environment

```bash
sudo nano /etc/logrotate.d/artworkfetcher
```
