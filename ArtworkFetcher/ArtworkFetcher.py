##################################################################
## ArtworkFetcher.py                                            ##
## a simple utility to fetch album artwork from last.fm         ##
## Developed Spring 2019 by Brady Bilderback                    ##
##################################################################

# standard imports
import os, json, sys, getopt, configparser, ast
from datetime import datetime, date, time, timedelta
from shutil import copyfile, SameFileError
from time import sleep

# imports for url and http requests
import urllib, urllib.parse, urllib.request

# import lastfm library
import pylast

class ArtworkFetcher:
    def __init__(self, config):
        # GENERAL
        self.check_interval     = int(config['GENERAL']['check_interval'])
        self.song_log_url       = config['GENERAL']['log_url']
        self.song_log_args      = ast.literal_eval(config['GENERAL']['log_args'])
        self.fetch_log          = config['GENERAL']['fetch_log']

        # LAST FM
        self.last_fm_username   = config['LAST_FM']['last_fm_user']
        self.last_fm_password   = config['LAST_FM']['last_fm_pass']
        self.last_fm_key        = config['LAST_FM']['last_fm_key']
        self.last_fm_secret     = config['LAST_FM']['last_fm_secret']

        # ARTWORK
        self.artwork_default    = config['ARTWORK']['artwork_default']
        self.artwork_default_url = config['ARTWORK']['artwork_default_url']
        self.artwork_output     = config['ARTWORK']['artwork_output']
        self.artwork_url_output = config['ARTWORK']['artwork_url_output']

    def _fetchArt(self, title, artist, album):

        lastfm = pylast.LastFMNetwork(
            api_key       = self.last_fm_key,
            api_secret    = self.last_fm_secret,
            username      = self.last_fm_username,
            password_hash = pylast.md5(self.last_fm_password))

        image = ""

        # fetch album artwork url from lastfm
        if album != "":
            try:
                lfm_album = lastfm.get_album(artist, album)
                image = lfm_album.get_cover_image()
            
            except (Exception, pylast.WSError) as error :
                print ("Error fetching album artwork! => ", error)

        # write out the image files
        if image == "" or image == "null" or image == None:
            # use the default url
            image = self.artwork_default_url

            # copy default image to artwork file
            try:
                copyfile(self.artwork_default, self.artwork_output)

            except (Exception, OSError) as e:
                print("Error => ", e)
            except (Exception, SameFileError) as e:
                print("Error => ", e)
        else:
            # fetch the image
            try:
                artwork_request = urllib.request.Request(image)
                artwork_data = urllib.request.urlopen(artwork_request).read()

            except (Exception, urllib.error.HTTPError) as e:
                print("HTTP Error => ", e)
            except (Exception, urllib.error.URLError) as e:
                print("URL Error => ", e)

            # write out album artwork file
            try:
                art_file = open(self.artwork_output, 'wb')
                art_file.write(artwork_data)
                art_file.close()

            except (Exception, IOError) as e:
                print("IO Error => ", e)

        # write to log
        try:
            log_file = open(self.fetch_log, 'a')
            log_file.write("Artwork: " + image + "\n")
            log_file.close()

        except (Exception, IOError) as e:
            print("IO Error => ", e)

        # create the json data for the artwork url
        url_data = {}
        url_data['timestamp'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        url_data['title']   = title
        url_data['artist']  = artist
        url_data['album']   = album
        url_data['url']     = image

        # write the artwork json data to file
        try:
            url_file = open(self.artwork_output_json, 'w')
            json.dump(url_data, url_file)
            url_file.close()
        
        except (Exception, IOError) as e:
            print("IO Error => ", e)
        
        return image

    def _fetchSong(self):
        # do the request for song data
        try:
            url = self.song_log_url + '?' + urllib.parse.urlencode(self.song_log_args)
            request = urllib.request.Request(url, headers={'User-Agent': 'ArtworkFetcher'})
            data = urllib.request.urlopen(request).read()
            data = json.loads(data)

            title     = data['songs'][0]['title']
            artist    = data['songs'][0]['artist']
            album     = data['songs'][0]['album']
            
        except (Exception, urllib.error.HTTPError) as e:
            print("HTTP Error => ", e)
        except (Exception, urllib.error.URLError) as e:
            print("URL Error => ", e)

        return { 'title': title, 'artist': artist, 'album': album }

    # function to infinitely check new artwork on each interval
    def runFetcher(self):
        # variables for current song data
        c_title     = ""
        c_artist    = ""
        c_album     = ""

        # check and fetch artwork
        while 1:
            # check if the current song is a new song
            new_song = self._fetchSong()

            if new_song['title'] != c_title or new_song['artist'] != c_artist or new_song['album'] != c_album:
                c_title     = new_song['title']
                c_artist    = new_song['artist']
                c_album     = new_song['album']

                # fetch the album artwork info
                c_artwork = self.fetchArt(c_title, c_artist, c_album)

                # log song info
                try:
                    log_file = open(self.fetch_log, 'a')
                    log_file.write("\n[ " + datetime.now().strftime('%Y-%m-%d %H:%M:%S') + " ]")
                    log_file.write("\nTitle:   " + c_title)
                    log_file.write("\nArtist:  " + c_artist)
                    log_file.write("\nAlbum:   " + c_album)
                    log_file.write("\nArtwork: " + c_artwork)
                    log_file.write("\n")
                    log_file.close()

                except (Exception, IOError) as e:
                    print("IO Error => ", e)

            sleep(self.check_interval)

        return True

if __name__ == "__main__":
    # fetch info from the config file
    try:
        opts, args = getopt.getopt(sys.argv[1:], "", ["config="])

    except (Exception, getopt.GetoptError):
        print("Specify a config file with --config=<file name>")
        sys.exit(1)

    if len(opts) < 1:
        print("Specify a file with --config=<file name>")
        sys.exit(1)

    config_file = None

    for o, a in opts:
        if o == "--config":
            config_file = a

    config = configparser.ConfigParser()
    config.read(config_file)
    pid_path = config['GENERAL']['pid_path']

    # write out a pid file
    # note that this will overwrite an existing pid file
    try:
        pid_file = open(pid_path, 'w')
        pid_file.write(str(os.getpid()) + "\n")
        pid_file.close()
        
    except (Exception, IOError) as e:
        print("IO Error => ", e)

    # make a new fetcher and run it
    fetcher = ArtworkFetcher(config)
    fetcher.runFetcher()

